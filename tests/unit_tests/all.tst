// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


// MB : 02/06/2010 : replaced the mprintf by a write(msprintf) to avoid a
// bug in the unit test system. :
// http://bugzilla.scilab.org/show_bug.cgi?id=7207

function mymprintf ( varargin )
  [lhs,rhs]=argn();
  if ( rhs == 1 ) then
    write(%io(2),msprintf(varargin(1)))
  elseif ( rhs == 2 ) then
    write(%io(2),msprintf(varargin(1),varargin(2)))
  elseif ( rhs == 3 ) then
    write(%io(2),msprintf(varargin(1),varargin(2),varargin(3)))
  else
    error ( " Unexpected number of input arguments" )
  end
endfunction

function [relerr,d] = significantDigits(a,b)
    relerr = norm(a-b)/max(norm(a),norm(b))
    dmax = floor(-log10(2^-53));
    if (relerr==0) then
        d = dmax
    else
        d = floor(-log10(relerr));
    end
endfunction

function checkProblem ( nprob , ord , tolg , tolgopt , tolH , stepH , evalues )

  mymprintf("Testing problem #%d\n",nprob)
  //
  mymprintf("  uncprb_getname...\n")
  name=uncprb_getname(nprob);
  assert_checkequal ( typeof(name) , "string" );
  mymprintf("  name=%s\n",name)
  //
  mymprintf("  uncprb_getinitf...\n")
  [n,m,x0]=uncprb_getinitf(nprob);
  assert_checkequal ( size(n) , [1 1] );
  assert_checkequal ( size(m) , [1 1] );
  assert_checkequal ( size(x0) , [n 1] );
  //
  mymprintf("  uncprb_getfunc, option=1...\n")
  option = 1;
  [fvec,J]=uncprb_getfunc(n,m,x0,nprob,option);
  assert_checkequal ( size(fvec) , [m 1] );
  assert_checkequal ( J , [] );
  //
  mymprintf("  uncprb_getfunc, option=2...\n")
  option = 2;
  [fvec,J]=uncprb_getfunc(n,m,x0,nprob,option);
  assert_checkequal ( fvec , [] );
  assert_checkequal ( size(J) , [m n] );
  //
  mymprintf("  uncprb_getfunc, option=3...\n")
  option = 3;
  [fvec,J]=uncprb_getfunc(n,m,x0,nprob,option);
  assert_checkequal ( size(fvec) , [m 1] );
  assert_checkequal ( size(J) , [m n] );
  //
  isHprovided = ( find(uncprb_getclass(4)==nprob) <> [] )
  if ( ~isHprovided ) then
    mymprintf("  Hessian not provided.\n")
  else
    mymprintf("  Hessian is provided.\n")
    mymprintf("  uncprb_getfunc, option=1...\n")
    option = 1;
    [fvec,J,H]=uncprb_getfunc(n,m,x0,nprob,option);
    assert_checkequal ( size(fvec) , [m 1] );
    assert_checkequal ( J , [] );
    assert_checkequal ( H , [] );
    //
    mymprintf("  uncprb_getfunc, option=2...\n")
    option = 2;
    [fvec,J,H]=uncprb_getfunc(n,m,x0,nprob,option);
    assert_checkequal ( fvec , [] );
    assert_checkequal ( size(J) , [m n] );
    assert_checkequal ( H , [] );
    //
    mymprintf("  uncprb_getfunc, option=3...\n")
    option = 3;
    [fvec,J,H]=uncprb_getfunc(n,m,x0,nprob,option);
    assert_checkequal ( size(fvec) , [m 1] );
    assert_checkequal ( size(J) , [m n] );
    assert_checkequal ( H , [] );
    //
    mymprintf("  uncprb_getfunc, option=4...\n")
    option = 4;
    [fvec,J,H]=uncprb_getfunc(n,m,x0,nprob,option);
    assert_checkequal ( size(fvec) , [m 1] );
    assert_checkequal ( size(J) , [m n] );
    assert_checkequal ( size(H) , [n n] );
    //
  end
  //
  mymprintf("  uncprb_getobjfcn...\n")
  f=uncprb_getobjfcn(n,m,x0,nprob);
  assert_checkequal ( size(f) , [1 1] );
  //
  mymprintf("  uncprb_getvecfcn...\n")
  fvec=uncprb_getvecfcn(n,m,x0,nprob);
  assert_checkequal ( size(fvec) , [m 1] );
  //
  mymprintf("  uncprb_getvecjac...\n")
  J=uncprb_getvecjac(n,m,x0,nprob);
  assert_checkequal ( size(J) , [m n] );
  //
  mymprintf("  uncprb_getgrdfcn...\n")
  g=uncprb_getgrdfcn(n,m,x0,nprob);
  assert_checkequal ( size(g) , [1 n] );
  //
  if ( isHprovided ) then
    mymprintf("  uncprb_gethesfcn...\n")
    H=uncprb_gethesfcn(n,m,x0,nprob);
    assert_checkequal ( size(H) , [n n] );
  end
  //
  mymprintf("  Check Jacobian by uncprb_getvecjac...\n")
  //
  Jstep = []
  Jfd = uncprb_getvecjacfd ( n , m , x0 , nprob , Jstep , ord );
  [relerr,d] = significantDigits(J,Jfd);
  mymprintf("  DIGITS(||J-Jfd||/||J||)=%d\n",d)
  // Comment the following for debug purposes:
  // allows to continue the script and get an overview of the situation
  assert_checkequal ( relerr < tolg , %t );
  if ( relerr > tolg ) then
    mymprintf ( "  Jacobian : FAIL.\n")
  else
    mymprintf ( "  Jacobian : ok.\n")
  end
  //
  mymprintf("  Check gradient by uncprb_getgrdfcn...\n")
  //
  gstep = []
  gfd = uncprb_getgrdfd ( n , m , x0 , nprob , gstep , ord );
  [relerr,d] = significantDigits(g,gfd);
  mymprintf("  DIGITS(||g-gfd||/||g||)=%d\n",d)
  // Comment the following for debug purposes
  // allows to continue the script and get an overview of the situation
  assert_checkequal ( relerr < tolg , %t );
  if ( relerr > tolg ) then
    mymprintf ( "  Gradient : FAIL.\n")
  else
    mymprintf ( "  Gradient : ok.\n")
  end
  //
  if ( isHprovided ) then
    mymprintf("  Check Hessian by uncprb_gethesfcn...\n")
    Hfd = uncprb_gethesfd ( n , m , x0 , nprob , stepH , 2 );
    [relerr,d] = significantDigits(H,Hfd);
    mymprintf("  DIGITS(||H-Hfd||/||H||)=%d\n",d)
    // Comment the following for debug purposes
    // allows to continue the script and get an overview of the situation
    assert_checkequal ( relerr < tolH , %t );
    if ( relerr > tolH ) then
      mymprintf ( "  Hessian : FAIL.\n")
    else
      mymprintf ( "  Hessian : ok.\n")
    end
  end
  //
  mymprintf("  Checking [norm(fvec) norm(J) f norm(g)]...\n");
  c = [norm(fvec) norm(J) f norm(g)];
  assert_checkalmostequal ( c , evalues , 1.e-7 );
  //
  mymprintf("  Checking uncprb_getopt : fopt/xopt...\n");
  [fopt,xopt] = uncprb_getopt(nprob,n,m);
  assert_checkequal ( size(fopt) , [1 1] );
  if ( xopt <> [] ) then
    mymprintf("  Size of x: expected: %d,   computed: %d\n",n,size(xopt,"*"));
    assert_checkequal ( size(xopt) , [n 1] );
  end
  if ( xopt <> [] ) then
    f2opt = uncprb_getobjfcn(n,m,xopt,nprob);
    mymprintf("  expected: %s,   computed: %s\n",string(fopt),string(f2opt));
    assert_checkalmostequal ( f2opt , fopt , 1.e-4 , 1.e-10);
    //
    mymprintf("  Checking g(xopt)=0...\n")
    gopt=uncprb_getgrdfcn(n,m,xopt,nprob);
    errrel = norm(gopt)/norm(g);
    mymprintf("  ||g(xopt)||/||g(x0)||=%s (tol=%s)...\n",string(errrel),string(tolgopt));
    assert_checkequal ( errrel<tolgopt , %t );
  end
  mymprintf("  Problem #%d ok.\n",nprob);
endfunction


// expected(nprob,1:4) = [norm(fvec) norm(J) f norm(g)]
expected = [
4.91934955D+00    2.60163844D+01    2.42000000D+01    2.32867688D+02
2.00124961D+01    3.45447973D+01    4.00500000D+02    1.27235372D+03
1.06548661D+00    1.00000001D+04    1.13526172D+00    2.00007356D+04
9.99999000D+05    1.73205081D+00    9.99998000D+11    2.00000000D+06
3.76870336D+00    3.74165739D+00    1.42031250D+01    2.77500000D+01
6.45856498D+01    7.31714042D+02    4.17130616D+03    9.37088183D+04
5.00000000D+01    1.88038979D+01    2.50000000D+03    1.87963549D+03
6.45613630D+00    6.74601056D+00    4.16816959D+01    8.46308181D+01
1.97182834D-03    1.89237216D+00    3.88810699D-06    7.45153281D-03
4.11534666D+04    1.06257188D+06    1.69360781D+09    8.72766933D+10
3.48004394D+00    8.51773333D+00    1.21107058D+01    3.97315969D+01
3.21115837D+01    2.49230970D+00    1.03115381D+03    1.49276374D+02
1.46628783D+01    1.79798185D+01    2.15000000D+02    4.58776634D+02
1.38535194D+02    6.08378783D+01    1.91920000D+04    1.63971256D+04
7.28915103D-02    1.71505869D+00    5.31317227D-03    1.34344066D-01
2.81543839D+03    4.31866431D+02    7.92669334D+06    2.14049067D+06
9.37564021D-01    2.37014075D+02    8.79026294D-01    4.18811512D+02
8.82649464D-01    3.49381401D+00    7.79070076D-01    2.55390136D+00
1.44686541D+00    5.32909204D+00    2.09341951D+00    5.89163519D+00
5.47722558D+00    2.56081653D+01    3.00000000D+01    1.77579104D+02
1.55563492D+01    2.60163844D+01    2.42000000D+02    7.36392287D+02
2.53968502D+01    1.79798185D+01    6.45000000D+02    7.94624440D+02
3.84750004D+02    3.92428339D+01    1.48032565D+05    3.01973609D+04
1.27535398D+01    1.96280473D+01    1.62652777D+02    5.00652174D+02
1.48275121D+03    1.51097684D+03    2.19855116D+06    4.48042693D+06
8.41175336D-02    8.71805797D-01    7.07575947D-03    9.91401433D-02
1.65302162D+01    1.04403083D+01    2.73248048D+02    3.44542450D+02
3.70808810D-02    3.91071765D+00    1.37499173D-03    5.82641488D-02
2.28682667D-01    1.27008303D+00    5.22957622D-02    5.65203420D-01
4.79583152D+00    9.91534578D+00    2.30000000D+01    5.16139516D+01
2.32379001D+01    2.22711029D+01    5.40000000D+02    1.02211545D+03
6.70820393D+00    1.00000000D+00    4.50000000D+01    1.26491106D+01
1.93334296D+03    6.90941387D+02    3.73781500D+06    2.67165212D+06
1.25602189D+03    4.82282075D+02    1.57759100D+06    1.21151181D+06
1.83747831D-01    5.53316222D+00    3.37632655D-02    1.33007265D+00
];

// Get the number of problems
nprobmax = uncprb_getproblems();
// Relative tolerance for the gradient
pb2tolg = 1.e-8 * ones(nprobmax,1);
// For these problems, 1.e-7 is too stringent
pb2tolg([17 35]) = 1.e-6;
// Map from problem number to finite difference order
// By default, order 2 should be sufficient
pb2order = 2 * ones(nprobmax,1);
// For the following problems, we need order 4
pb2order([4 17 31]) = 4;
// For the following problems, we order 2 is too expensive
pb2order([35]) = 1;
// Map from problem number to relative tolerance on gopt
pb2tolgopt = 1.e-8 * ones(nprobmax,1);
// These problems are really badly scaled
pb2tolgopt(3) = 1.e-2;
pb2tolgopt(6) = 1.e-4;
pb2tolgopt(8) = 1.e-6;
pb2tolgopt(15) = 1.e-5;
pb2tolgopt(19) = 1.e-7;
pb2tolgopt(26) = 1.e-7;
//
tolH = 1.e-4;
// Step for the finite difference Hessian
pb2Hstep = list();
for i = 1 : nprobmax
  pb2Hstep (i) = [];
end
pb2Hstep(4) = 1;
pb2Hstep(35) = 1.e-5;

//
// Check all tests
for nprob = 1 : nprobmax
  ord = pb2order(nprob);
  tolg = pb2tolg(nprob);
  tolgopt = pb2tolgopt(nprob);
  evalues = expected(nprob,1:4);
  stepH = pb2Hstep(nprob);
  //
  checkProblem ( nprob , ord , tolg , tolgopt , tolH , stepH , evalues )
end


