// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


// Reconstitute the matrix :
// H = [
//  1  2  3  4
//  2  6  7  8
//  3  7 11 12
//  4  8 12 16
// ]
//
H = [
  1  2  3  4
  2  6  7  8
  3  7 11 12
  4  8 12 16
 ];
[hesd , hesl] = uncprb_H2hesdl ( H );
Ed = [1 6 11 16]';
El = [2 3 7 4 8 12]';
assert_checkequal ( hesd , Ed );
assert_checkequal ( hesl , El );

