// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Use default
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
Jfd = uncprb_getvecjacfd ( n , m , x0 , nprob );
J = [
24 10
-1 0
];
assert_checkalmostequal ( Jfd , J , 1.e-10 );

// Use default
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
Jfd = uncprb_getvecjacfd ( n , m , x0 , nprob , [] , [] );
J = [
24 10
-1 0
];
assert_checkalmostequal ( Jfd , J , 1.e-10 );

// Set step
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
Jfd = uncprb_getvecjacfd ( n , m , x0 , nprob , 1.e-1 );
J = [
24 10
-1 0
];
assert_checkalmostequal ( Jfd , J , 1.e-15 );

// Set step and order
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
Jfd = uncprb_getvecjacfd ( n , m , x0 , nprob , 1.e-1 , 4 );
J = [
24 10
-1 0
];
assert_checkalmostequal ( Jfd , J , 1.e-14 );

// Set order (use default step)
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
Jfd = uncprb_getvecjacfd ( n , m , x0 , nprob , [] , 4 );
J = [
24 10
-1 0
];
assert_checkalmostequal ( Jfd , J , 1.e-12 );

