// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Make optim optimize the problem #1
nprob = 1;
geval = [];
iter = [];
[n,m,x0]=uncprb_getinitf(nprob);
uncprb_optiminit();
[fopt,xopt,gopt]=optim(list(uncprb_optimfun,nprob,n,m),x0,"gc");
funeval = uncprb_optimget();
rtolX = 1.e-4;
atolX = 1.e-10;
rtolF = 1.e-4;
atolF = 1.e-10;
//
// Format "detailed"
success = uncprb_printsummary("optim/UNC/gc", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
rtolF, atolF, rtolX, atolX, "detailed");
assert_checktrue(success);
//
// Format "line"
success = uncprb_printsummary("optim/UNC/gc", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
rtolF, atolF, rtolX, atolX, "line");
assert_checktrue(success);
//
// Format "wiki"
success = uncprb_printsummary("optim/UNC/gc", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
rtolF, atolF, rtolX, atolX, "wikihead");
success = uncprb_printsummary("optim/UNC/gc", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
rtolF, atolF, rtolX, atolX, "wiki");
assert_checktrue(success);
