// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

[prb_n,prb_m] = uncprb_getvarprbs();
assert_checkequal ( typeof(prb_n) , "constant" );
assert_checkequal ( size(prb_n,"c") , 1 );
assert_checkequal ( size(prb_n,"r") > 1, %t );
assert_checkequal ( typeof(prb_m) , "constant" );
assert_checkequal ( size(prb_m,"c") , 1 );
assert_checkequal ( size(prb_m,"r") > 1, %t );

