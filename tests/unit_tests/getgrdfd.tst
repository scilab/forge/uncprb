// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Use default
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
gfd = uncprb_getgrdfd ( n , m , x0 , nprob );
g = [ -215.6 -88];
assert_checkalmostequal ( gfd , g , 1.e-10 );

// Use default
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
gfd = uncprb_getgrdfd ( n , m , x0 , nprob , [] , [] );
g = [ -215.6 -88];
assert_checkalmostequal ( gfd , g , 1.e-10 );

// Set step
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
gfd = uncprb_getgrdfd ( n , m , x0 , nprob , 4.e-6 );
g = [ -215.6 -88];
assert_checkalmostequal ( gfd , g , 1.e-11 );

// Set step and order
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
gfd = uncprb_getgrdfd ( n , m , x0 , nprob , 1.e0 , 4 );
g = [ -215.6 -88];
assert_checkalmostequal ( gfd , g , 1.e-15 );

// Set order (use default step)
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
gfd = uncprb_getgrdfd ( n , m , x0 , nprob , [] , 4 );
g = [ -215.6 -88];
assert_checkalmostequal ( gfd , g , 1.e-12 );

