// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Check that the wrapper works fine.
clearglobal();
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
[f0,g0,ind]=uncprb_optimfun(x0,4,nprob,n,m);
assert_checkalmostequal(f0,24.2);
assert_checkalmostequal(g0,[-215.6 -88]);

// Check that the wrapper works fine.
// Get the number of iterations, function and gradient evaluations
clearglobal();
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
uncprb_optiminit();
// Call F: funeval + 1
[f0,g0,ind]=uncprb_optimfun(x0,4,nprob,n,m);
assert_checkalmostequal(f0,24.2);
assert_checkalmostequal(g0,[-215.6 -88]);
funeval = uncprb_optimget();
assert_checkequal(funeval,1);

// Make optim optimize the problem #1
clearglobal();
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
uncprb_optiminit();
objfun= list(uncprb_optimfun,nprob,n,m);
[fopt,xopt,gopt]=optim(objfun,x0);
[fstar,xstar] = uncprb_getopt(nprob,n,m);
assert_checkequal(fstar,fopt);
assert_checkequal(xstar,xopt);
goptE = uncprb_getgrdfcn(n,m,xstar,nprob);
assert_checkequal(goptE',gopt);

// Make optim optimize the problem #1
// Get the number of function evaluations
clearglobal();
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
uncprb_optiminit();
objfun= list(uncprb_optimfun,nprob,n,m);
[fopt,xopt,gopt]=optim(objfun,x0);
[fstar,xstar] = uncprb_getopt(nprob,n,m);
assert_checkequal(fstar,fopt);
assert_checkequal(xstar,xopt);
goptE = uncprb_getgrdfcn(n,m,xstar,nprob);
assert_checkequal(goptE',gopt);
funeval = uncprb_optimget();
assert_checkequal(funeval,46);
