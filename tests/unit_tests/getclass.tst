// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->



nprobmax = uncprb_getproblems();
//
prbmat = uncprb_getclass(1);
assert_checkequal ( size(prbmat) , [1 14] );
assert_checkequal ( find(prbmat<1) , [] );
assert_checkequal ( find(prbmat>nprobmax) , [] );
//
prbmat = uncprb_getclass(2);
assert_checkequal ( size(prbmat) , [1 18] );
assert_checkequal ( find(prbmat<1) , [] );
assert_checkequal ( find(prbmat>nprobmax) , [] );
//
prbmat = uncprb_getclass(3);
assert_checkequal ( size(prbmat) , [1 18] );
assert_checkequal ( find(prbmat<1) , [] );
assert_checkequal ( find(prbmat>nprobmax) , [] );
//
prbmat = uncprb_getclass(1);
uncprb_getproblems(prbmat);

