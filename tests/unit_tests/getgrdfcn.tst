// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Get gradient at x0 for Rosenbrock's test case
n = 2;
m = 2;
x = [-1.2,1]';
g=uncprb_getgrdfcn(n,m,x,1);
assert_checkalmostequal ( g , [-215.6,-88], %eps );

// Get gradient at x* for Rosenbrock's test case
n = 2;
m = 2;
x = [1,1]';
g=uncprb_getgrdfcn(n,m,x,1);
assert_checkalmostequal ( g , [0 0], %eps );


