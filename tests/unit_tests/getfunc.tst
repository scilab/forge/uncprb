// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->


// Get fvec and J at x0 for Rosenbrock's test case
n = 2;
m = 2;
x = [-1.2,1]';
option = 3;
nprob = 1;
[fvec,J]=uncprb_getfunc(n,m,x,nprob,option);
assert_checkalmostequal ( fvec , [-4.4 2.2]', %eps );
Je = [
    24.    10.
  - 1.     0.
];
assert_checkalmostequal ( J , Je, %eps );

// See what happens if option=1
n = 2;
m = 2;
x = [-1.2,1]';
option = 1;
nprob = 1;
[fvec,J]=uncprb_getfunc(n,m,x,nprob,option);
assert_checkalmostequal ( fvec , [-4.4 2.2]', %eps );
assert_checkequal ( J , [] );

// See what happens if option=2
n = 2;
m = 2;
x = [-1.2,1]';
option = 2;
nprob = 1;
[fvec,J]=uncprb_getfunc(n,m,x,nprob,option);
assert_checkequal ( fvec , [] );
Je = [
    24.    10.
  - 1.     0.
];
assert_checkalmostequal ( J , Je, %eps );


