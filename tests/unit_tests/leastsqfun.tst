// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// Check that the wrapper works fine.
clearglobal();
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
r0=uncprb_leastsqfun(x0, m, nprob, n);
rexp = [-4.4;2.2];
assert_checkalmostequal(r0,rexp);

// Check that the wrapper works fine.
// Get the number of iterations, function and gradient evaluations
clearglobal();
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
uncprb_leastsqinit();
// Call F: funeval + 1
r0=uncprb_leastsqfun(x0, m, nprob, n);
rexp = [-4.4;2.2];
assert_checkalmostequal(r0,rexp);
funeval = uncprb_leastsqget();
assert_checkequal(funeval,1);

// Make leastsq optimize the problem #1
clearglobal();
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
uncprb_leastsqinit();
objfun = list(uncprb_leastsqfun, m, nprob, n);
[fopt,xopt,gopt]=leastsq(objfun,x0);
[fstar,xstar] = uncprb_getopt(nprob,n,m);
assert_checkequal(fstar,fopt);
assert_checkequal(xstar,xopt);
goptE = uncprb_getgrdfcn(n,m,xstar,nprob);
assert_checkequal(goptE',gopt);

// Make leastsq optimize the problem #1
// Get the number of function evaluations
clearglobal();
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
uncprb_leastsqinit();
objfun = list(uncprb_leastsqfun, m, nprob, n);
[fopt,xopt,gopt]=leastsq(objfun,x0);
[fstar,xstar] = uncprb_getopt(nprob,n,m);
assert_checkequal(fstar,fopt);
assert_checkequal(xstar,xopt);
goptE = uncprb_getgrdfcn(n,m,xstar,nprob);
assert_checkequal(goptE',gopt);
funeval = uncprb_leastsqget();
assert_checkequal(funeval,184);

// Make lsqrsolve optimize the problem #1
// With Jacobian
// Get the number of iterations, function and gradient evaluations
nprob = 1;
[n,m,x0]=uncprb_getinitf(nprob);
uncprb_leastsqinit();
objfun = list(uncprb_leastsqfun, m, nprob, n);
[fopt,xopt,gopt]=leastsq(objfun,uncprb_leastsqjac,x0)
[foptE,xoptE] = uncprb_getopt(nprob,n,m);
goptE = uncprb_getgrdfcn(n,m,xoptE,nprob);
[funeval,Jeval] = uncprb_leastsqget();
assert_checkequal(funeval,46);
assert_checkequal(Jeval,46);
