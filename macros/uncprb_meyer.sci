// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_meyer(n,m,x,option)

    // ************************************************
    // ************************************************
    // function [fvec,J]= meyer(n,m,x,option)
    // Meyer function   [10]
    // Dimensions   n=3   m=16
    // Function definition:
    //       f(x) = x(1)*exp[x(2)/(t(i) + x(3))]-y(i)
    //       where t(i)= 45 + 5i
    // Standard starting point at x=(0.02,4000,250)
    // Minima of f=87.9458...
    //

    if ( option<1 | option>3 ) then
        msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_meyer",option);
    end
    //
    fvec=[]
    J=[]
    //
    y = [34780;28610;23650;19630;16370;13720;11540;9744;8261;7030;6005;5147;4427;3820;3307;2872]';

    x1 = x(1);
    x2 = x(2);
    x3 = x(3);

    if option==1 then
        for i = 1:m
            ti = 45+5*i;
            di = ti+x3;
            ei = exp(x2/di);
            fvec(i) = x1*ei-y(i);
        end

    elseif option==2 then
        for i = 1:m
            ti = 45+5*i;
            di = ti+x3;
            qi = 1/di;
            ei = exp(x2*qi);
            si = x1*qi*ei;
            J(i,1) = ei;
            J(i,2) = si;
            J(i,3) = -x2*qi*si;
        end

    elseif option==3 then
        for i = 1:m
            ti = 45+5*i;
            di = ti+x3;
            qi = 1/di;
            ei = exp(x2*qi);
            si = x1*qi*ei;
            fvec(i) = x1*ei-y(i);
            J(i,1) = ei;
            J(i,2) = si;
            J(i,3) = -x2*qi*si;
        end
    end
endfunction
function apifun_checktrue ( var , msg )
    if ( ~var ) then
        error(msg);
    end
endfunction

