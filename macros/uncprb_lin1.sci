// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_lin1(n,m,x,option)
  // **********************************
  // Function [fvec,J] = lin1(n,m,x,option)
  // Linear function - rank 1   [33]
  // Dimensions:  n variable,    m>=n
  // Standard starting point: (1,....,1)
  // Minima of f=[(m(m-1))/(2(2m+1))]
  //
  // Coded in MATLAB    11/94      PLK
  // **********************************

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_lin1",option);
  end
  //
  apifun_checktrue ( m>=n , msprintf(gettext("%s: Expected m>=n, but got the contrary. m=%d, n=%d"), "uncprb_lin1" , m , n ))
  //
  fvec=[]
  J=[]
  //
  if ( option==2 | option==3 ) then
    J = zeros(m,n);
  end
  for i = 1:m
    sum1 = 0;
    for j = 1:n
      sum1 = sum1+j*x(j);
    end

    if ( option==1 | option==3 ) then
      fvec(i) = i*sum1-1;
    end

    if ( option==2 | option==3 ) then

      for j = 1:n
        J(i,j) = i*j;
      end

    end
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

