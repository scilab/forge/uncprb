// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_osb2(n,m,x,option)
  //
  //  Function [fvec,J] = osb2(n,m,x,option)
  //  Osborne 2 function      [19]
  //  Dimensions:  n=11, m=65
  //  Standard starting point: (1.3,0.65,0.7,0.6,3,5,7,2,4.5,5.5)
  //  Minima of f=4.01377...10^(-2)
  //  at (1.31,.4315,.6336,.5993,.7539,.9056,1.3651,4.8248,2.3988,
  //       4.5689,5.6754)
  //
  //  Revised   11/94              Plk
  // MB : vectorized

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_osb2",option);
  end
  //
  fvec=[]
  J=[]
  //
  y = [1.366,1.191,1.112,1.013,0.991,0.885,0.831,0.847,0.786,0.725,0.746,0.679,0.608,0.655,0.616,0.606,0.602,0.626,0.651,0.724,0.649,0.649,0.694,0.644,0.624,0.661,0.612,0.558,0.533,0.495,0.5,0.423,0.395,0.375,0.372,0.391,0.396,0.405,0.428,0.429,0.523,0.562,0.607,0.653,0.672,0.708,0.633,0.668,0.645,0.632,0.591,0.559,0.597,0.625,0.739,0.71,0.729,0.72,0.636,0.581,0.428,0.292,0.162,0.098,0.054]';
  //
  t(1:m) = ((1:m)'-1)/10;
  t09 = t-x(9);
  t10 = t-x(10);
  t11 = t-x(11);
  s09 = t09.^2;
  s10 = t10.^2;
  s11 = t11.^2;
  e1 = exp(-t*x(5));
  e2 = exp(-s09*x(6));
  e3 = exp(-s10*x(7));
  e4 = exp(-s11*x(8));
  //
  if ( option==1 | option==3 ) then
    fvec(1:m) = x(1)*e1(1:m)+x(2)*e2(1:m)+x(3)*e3(1:m)+x(4)*e4(1:m)-y(1:m);
  end
  if ( option==2 | option==3 ) then
    r2 = x(2)*e2;
    r3 = x(3)*e3;
    r4 = x(4)*e4;
    J(1:m,1:11) = [e1,e2,e3,e4,-t*x(1).*e1,-s09.*r2,-s10.*r3,-s11.*r4,2*t09*x(6).*r2,2*t10*x(7).*r3,2*t11*x(8).*r4];
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

