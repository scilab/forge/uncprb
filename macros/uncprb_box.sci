// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_box(n,m,x,option)

  // *****************************************************************
  // *****************************************************************
  // Function [fvec,J] = box(n,m,x,option)
  // Box three-dimensional function      [12]
  // Dimensions:   n=3     m=10
  // Function definition:
  //       f(x)= exp[-t(i)x1]-exp[-t(i)x2]-x3[exp[-t(i)]-exp[-10t(i)]]
  //       where t(i)=(0.1)i
  // Standard Starting Points: (0,10,20)
  // Minima of f=0 at (1,10,1), (10,1,-1) and wherever x1=x2 and x3-0
  // ******************************************************************

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_box",option);
  end
  //
  apifun_checktrue ( m>=n , msprintf(gettext("%s: Expected m>=n, but got the contrary. m=%d, n=%d"), "uncprb_box" , m , n ))
  //
  fvec=[]
  J=[]
  H = []
  //
  for i = 1:m

    t(1,i) = 0.1*i;

    if ( option==1 | option==3 | option==4 ) then
      fvec(i) = exp(-t(i)*x(1))-exp(-t(i)*x(2))-x(3)*(exp(-t(i))-exp(-10*t(i)));
    end
    //
    if ( option==2 | option==3 | option==4 ) then
      J(i,1) = -t(i)*exp(-t(i)*x(1));
      J(i,2) = t(i)*exp(-t(i)*x(2));
      J(i,3) = -(exp(-t(i))-exp(-10*t(i)));
    end
  end
  //
  if ( option==4 ) then
    hesd(1:3) = 0
    hesl(1:3) = 0
    for i = 1: 10
      d1 = i
      d2 = d1/10
      s1 = exp(-d2*x(1))
      s2 = exp(-d2*x(2))
      s3 = exp(-d2) - exp(-d1)
      t = s1 - s2 - s3*x(3)
      th = t*d2**2
      hesd(1) = hesd(1) + th*s1 + (d2*s1)**2
      hesd(2) = hesd(2) - th*s2 + (d2*s2)**2
      hesd(3) = hesd(3) + s3**2
      hesl(1) = hesl(1) - s1*s2*d2**2
      hesl(2) = hesl(2) + d2*s1*s3
      hesl(3) = hesl(3) - d2*s2*s3
    end
    hesd(1) = 2*hesd(1)
    hesd(2) = 2*hesd(2)
    hesd(3) = 2*hesd(3)
    hesl(1) = 2*hesl(1)
    hesl(2) = 2*hesl(2)
    hesl(3) = 2*hesl(3)
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

