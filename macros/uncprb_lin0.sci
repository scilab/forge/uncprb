// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_lin0(n,m,x,option)

  // Function[fvec,J] = lin0(n,m,x,option)
  // Linear function - rank 1 with zero columns and rows  [34]
  // Dimensions: n variable,     m>=n
  // Standard starting point: (1,...1)
  // Minima of f=(m^2 + 3m - 6)/2(2m - 3)

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_lin0",option);
  end
  //
  apifun_checktrue ( m>=n , msprintf(gettext("%s: Expected m>=n, but got the contrary. m=%d, n=%d"), "uncprb_lin0" , m , n ))
  //
  fvec=[]
  J=[]
  //
  if ( option==2 | option==3 ) then
    J = zeros(m,n);
  end
  total = sum ( (2:n-1).*x(2:n-1)' )
  if ( option==1 | option==3 ) then
    fvec(2:m-1) = ((2:m-1)-1)'.*total-1;
  end
  if ( option==2 | option==3 ) then
    J(2:m-1,2:n-1) = ((2:m-1) - 1)' * (2:n-1)
  end

  if ( option==1 | option==3 ) then
    fvec(1) = -1
    fvec(m) = -1
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

