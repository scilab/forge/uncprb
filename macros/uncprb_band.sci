// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_band(n,m,x,option)


  // Function [Fvec, J]= band (n,m,x,option)
  // Broyden banded function   [31]
  // Dimensions: n variable,   m=n
  // Standard starting point: (-1,...,-1)
  // minima of f=0
  // MB : vectorized statements and fixed derivative.

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_band",option);
  end
  //
  apifun_checktrue ( m==n , msprintf(gettext("%s: Expected m==n, but got the contrary. m=%d, n=%d"), "uncprb_band" , m , n ))
  //
  fvec=[]
  J=[]
  //
  ml = 5;
  mu = 1;

  if ( option==2 | option==3 ) then
    J = zeros(m,n);
  end
  if ( option==1 | option==3 ) then
    ml = 5
    mu = 1
    for k = 1:n
      k1 = max(1,k-ml)
      k2 = min(k+mu,n)
      ik = k1:k2
      notk = find(ik<>k)
      temp = sum ( x(ik(notk)).*(1 + x(ik(notk))) )
      fvec(k) = x(k)*(2 + 5*x(k)**2) + 1 - temp
    end
  end
  if ( option==2 | option==3 ) then
    ml = 5
    mu = 1
    for k = 1:n
      k1 = max(1,k-ml)
      k2 = min(k+mu,n)
      ik = k1:k2
      notk = find(ik<>k)
      J(k,ik(notk)) = -(1 + 2*x(ik(notk)))'
      J(k,k) = 2 + 15*x(k)**2
    end
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

