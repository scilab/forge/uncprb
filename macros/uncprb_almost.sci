// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_almost(n,m,x,option)

  // BROWN ALMOST-LINEAR FUNCTION.
  // Problem no. 27
  // Dimensions -> n=variable, m=n
  // Standard starting point -> x=(1/2,1/2,...,1.2)
  // Minima -> f=0 at (a,...,a,a^(1-n)) where a satisfies
  // na^n-(n+1)a^(n-1)+1=0. In particular f=1 at (0,...,0,n+1)
  // MB : vectorized statements and fixed derivative.

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_almost",option);
  end
  //
  apifun_checktrue ( m==n , msprintf(gettext("%s: Expected m==n, but got the contrary. m=%d, n=%d"), "uncprb_almost" , m , n ))
  //
  fvec=[]
  J=[]
  //
  if ( option==1 | option==3 ) then
    sx = sum(x)-(n+1)
    fvec(1:n-1) = x(1:n-1) + sx
    fvec(n) = prod(x) - 1
  end
  if ( option==2 | option==3 ) then
    pr = prod(x)
    J = ones(n,n) + eye()
    for j = 1:n
      if (x(j) <> 0) then
        J(n,j) = pr/x(j)
      else
        notj = find(j<>(1:n))
        J(n,j) = prod(x(notj))
      end
    end
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

