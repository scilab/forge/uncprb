// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function uncprb_optiminit()
    // Initialize the internal data for uncprb_optimfun
    //
    // Calling Sequence
    //   uncprb_optiminit()
    //
    // Parameters
    //
    //
    // Description
    // Initialize the internal data used by uncprb_optimfun.
    //
    // Examples
    //   // See uncprb_optimfun
    //
    // Authors
    //   Michael Baudin - 2011 - DIGITEO

    global __OPTIMFUNDATA__

    __OPTIMFUNDATA__.funeval = 0
endfunction

