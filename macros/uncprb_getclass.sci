// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function prbmat = uncprb_getclass(class)
  // Returns the name.
  //
  // Calling Sequence
  //   prbmat = uncprb_getclass(class)
  //
  // Parameters
  //   class: a floating point integer, class = 1 for Systems of Nonlinear Equations, class=2 for Nonlinear Least Squares, class=3 for Unconstrained Minimization, class=4 for Hessian-provided problems
  //   prbmat: a 1 x p matrix of floating point integers, the problem numbers
  //
  // Description
  // Returns the problem numbers which correspond to the given class,
  // as defined by the paper. The numbers are returned
  // as a row vector, so that it can be directly used for a loop.
  //
  // The order of the problems in the set is the one of the paper.
  //
  // Examples
  //   // Get all nonlinear equation problems
  //   prbmat = uncprb_getclass(1)
  //
  //   // Get all nonlinear least squares problems
  //   prbmat = uncprb_getclass(1)
  //
  //   // Get all unconstrained minimization problems
  //   prbmat = uncprb_getclass(3)
  //
  //   // Get all problems for which the Hessian is provided
  //   prbmat = uncprb_getclass(4)
  //
  //   // Get all unconstrained minimization problems for which the Hessian is provided
  //   prbmat = intersect(uncprb_getclass(3),uncprb_getclass(4))
  //
  //   // See if the Hessian is available for problem #nprob
  //   nprob = 1
  //   isHessian = ( find(uncprb_getclass(4)==nprob) <> [] )
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO

  [lhs,rhs]=argn();
  if ( rhs <> 1 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 1 are expected."), "uncprb_getclass", rhs);
    error(errmsg)
  end
  //
  if ( class==1 ) then
    prbmat = [1 13 3 14 7 20 35 27 28 29 26 25 30 31]
  elseif ( class==2 ) then
    prbmat = [32 33 34 1 7 13 2 8 15 10 20 12 6 16 35 27 17 19]
  elseif ( class==3 ) then
    prbmat = [7 18 9 3 12 25 20 23 24 4 16 11 29 21 22 5 14 35]
  elseif ( class==4 ) then
    prbmat = [1  3  4  5  7  9  11  12  14  16  18  20  21  22  23  24  25  26  35]
  else
    errmsg = msprintf(gettext("%s: Unexpected class : %d provided while 1 to 4 is expected."), "uncprb_getclass", class);
    error(errmsg)
  end
endfunction

