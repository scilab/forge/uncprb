// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_trig(n,m,x,option)

  // Trigonometric function
  // ----------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=trig(n,m,x,option)
  // Problem no. 26
  // Dimensions -> n=variable, m=n
  // Standard starting point -> x=(1/n,..,1/n)
  // Minima -> f=0
  //
  // 11/21/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // TODO : vectorize

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_trig",option);
  end
  //
  apifun_checktrue ( m==n , msprintf(gettext("%s: Expected m==n, but got the contrary. m=%d, n=%d"), "uncprb_trig" , m , n ))
  //
  fvec=[];
  J=[];
  H=[]
  //
  if ( option==1 | option==3 | option==4 ) then
    sum1 = 0;
    for i = 1:n
      xi = x(i);
      cxi = cos(xi);
      sum1 = sum1+cxi;
      fvec(i) = n+i*(1-cxi)-sin(xi);
    end
    fvec = fvec-sum1;
  end
  //
  if ( option==2 | option==3 | option==4 ) then
    for j = 1:n
      xj = x(j);
      sxj = sin(xj);
      for i = 1:n
        J(i,j) = sxj;
      end
      J(j,j) = (j+1)*sxj-cos(xj);
    end
  end
  //
  if ( option==4 ) then
    hesd(1: n) = sin(x(1: n))
    s1 = sum ( cos(x(1: n)) )
    s2 = 0
    im = 0
    for j = 1: n
      th = cos(x(j))
      t = (n+j) - hesd(j) - s1 - (j)*th
      s2 = s2 + t
      for k = 1: j-1
        im = im + 1
        hesl(im) = sin(x(k))*((n+j+k)*hesd(j)-th) - hesd(j)*cos(x(k))
        hesl(im) = 2*hesl(im)
      end
      hesd(j) = (j*(j+2)+n)*hesd(j)**2 + th*(th-(2*j+2)*hesd(j)) + t*((j)*th+hesd(j))
    end
    hesd(1: n) = 2*(hesd(1: n) + cos(x(1: n))*s2)
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

