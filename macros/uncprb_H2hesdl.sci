// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [hesd , hesl] = uncprb_H2hesdl ( H )
  // Reconstitute the hesd and hesl from Hessian matrix H.
  // hesd : a column matrix, the diagonal part of the Hessian
  // hesl : a column matrix, the lower part terms, row by row.
  n = size(H,"r")
  hesd = diag(H)
  kc = 1
  k1 = 1
  k2 = 1
  for i = 2 : n
    hesl(k1:k2) = H ( 1:i-1 , i )
    kc = kc + 1
    k1 = k2 + 1
    k2 = k1 + kc - 1
  end
endfunction

