// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_biggs(n,m,x,option)

  // ******************************************
  //  function [fvec,J] = biggs(n,m,x,option)
  //  Biggs EXP6 function   [18]
  //  Dimensions :  n=6,  m=13
  //  Standard starting point (1,2,1,1,1,1)
  //  Minima of f=5.65565...10^(-3)   if m=13
  //            f=0 at (1,10,1,5,4,3)
  //
  //  Revised  11/94               PLK
  // ******************************************

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_biggs",option);
  end
  //
  apifun_checktrue ( m>=n , msprintf(gettext("%s: Expected m>=n, but got the contrary. m=%d, n=%d"), "uncprb_biggs" , m , n ))
  //
  fvec=[]
  J=[]
  H = []
  //
  if ( option==2 | option==3 | option==4 ) then
    J = zeros(m,n);
  end

  for i = 1:m
    t(1,i) = 0.1*i;
    y(1,i) = exp(-t(i))-5*exp(-10*t(i))+3*exp(-4*t(i));

    if ( option==1 | option==3 | option==4 ) then
      fvec(i) = x(3)*exp(-t(i)*x(1))-x(4)*exp(-t(i)*x(2))+x(6)*exp(-t(i)*x(5))-y(i);
    end

    if ( option==2 | option==3 | option==4 ) then
      J(i,1) = -t(i)*x(3)*exp(-t(i)*x(1));
      J(i,2) = t(i)*x(4)*exp(-t(i)*x(2));
      J(i,3) = exp(-t(i)*x(1));
      J(i,4) = -exp(-t(i)*x(2));
      J(i,5) = x(6)*(-t(i))*exp(-t(i)*x(5));
      J(i,6) = exp(-t(i)*x(5));
    end
  end
  //
  if ( option==4 ) then
    hesd(1:6) = 0
    hesl(1:15) = 0
    for i = 1: 13
      d1 = i/10
      d2 = exp(-d1) - 5*exp(-10*d1) + 3*exp(-4*d1)
      s1 = exp(-d1*x(1))
      s2 = exp(-d1*x(2))
      s3 = exp(-d1*x(5))
      t = x(3)*s1 - x(4)*s2 + x(6)*s3 - d2
      d2 = d1**2
      s1s2 = s1 * s2
      s1s3 = s1 * s3
      s2s3 = s2 * s3
      hesd(1) = hesd(1) + d2*s1*(t+x(3)*s1)
      hesd(2) = hesd(2) - d2*s2*(t-x(4)*s2)
      hesd(3) = hesd(3) + s1**2
      hesd(4) = hesd(4) + s2**2
      hesd(5) = hesd(5) + d2*s3*(t+x(6)*s3)
      hesd(6) = hesd(6) + s3**2
      hesl(1) = hesl(1) - d2*s1s2
      hesl(2) = hesl(2) - d1*s1*(t+x(3)*s1)
      hesl(3) = hesl(3) + d1*s1s2
      hesl(4) = hesl(4) + d1*s1s2
      hesl(5) = hesl(5) + d1*s2*(t-x(4)*s2)
      hesl(6) = hesl(6) - s1s2
      hesl(7) = hesl(7) + d2*s1s3
      hesl(8) = hesl(8) - d2*s2s3
      hesl(9) = hesl(9) - d1*s1s3
      hesl(10) = hesl(10) + d1*s2s3
      hesl(11) = hesl(11) - d1*s1s3
      hesl(12) = hesl(12) + d1*s2s3
      hesl(13) = hesl(13) + s1s3
      hesl(14) = hesl(14) - s2s3
      hesl(15) = hesl(15) - d1*s3*(t+x(6)*s3)
    end
    hesd(1) = x(3)*hesd(1)
    hesd(2) = x(4)*hesd(2)
    hesd(5) = x(6)*hesd(5)
    hesl(1) = x(3)*x(4)*hesl(1)
    hesl(3) = x(4)*hesl(3)
    hesl(4) = x(3)*hesl(4)
    hesl(7) = x(3)*x(6)*hesl(7)
    hesl(8) = x(4)*x(6)*hesl(8)
    hesl(9) = x(6)*hesl(9)
    hesl(10) = x(6)*hesl(10)
    hesl(11) = x(3)*hesl(11)
    hesl(12) = x(4)*hesl(12)
    //
    hesd(1: 6) = 2*hesd(1: 6)
    hesl(1: 15) = 2*hesl(1: 15)
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end

endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

