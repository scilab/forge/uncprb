// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_wood(n,m,x,option)


  // Function [fvec,J]=WOOD (n,m,x,option)
  // Wood function    [14]
  // Dimensions:     n=4   m=6
  // Function Definition:
  //       f1(x)= 10(x2 -x1^2)
  //       f2(x)= 1 - x1
  //       f3(x)= (90)^.5*(x4-x3^2)
  //       f4(x)= 1-x3
  //       f5(x)= (10)^.5 * (x2+x4-2)
  //       f6(x)= (10)^(-.5) * (x2-x4)
  // Standard starting point:  (-3,-1,-3,-1)
  // Minima of f=0 at (1,1,1,1)
  // *********************************************x


  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_wood",option);
  end
  //
  fvec=[];
  J=[];
  H=[]
  //
  if ( option==1 | option==3 | option==4 ) then
    fvec = [10*(x(2)-(x(1)^2));1-x(1);sqrt(90)*(x(4)-(x(3)^2));1-x(3);sqrt(10)*(x(2)+x(4)-2);1/sqrt(10)*(x(2)-x(4))];
  end
  //
  if ( option==2 | option==3 | option==4 ) then
    J = [-20*x(1),10,0,0;-1,0,0,0;0,0,-2*sqrt(90)*x(3),sqrt(90);0,0,-1,0;0,sqrt(10),0,sqrt(10);0,1/sqrt(10),0,-1/sqrt(10)];
  end
  //
  if ( option==4 ) then
    hesd(1) = 1200*x(1)**2 - 400*x(2) + 2
    hesd(2) = 220.2
    hesd(3) = 1080*x(3)**2 - 360*x(4) + 2
    hesd(4) = 200.2
    hesl(1) = -400*x(1)
    hesl(2) = 0
    hesl(3) = 0
    hesl(4) = 0
    hesl(5) = 19.8
    hesl(6) = -360*x(3)
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

