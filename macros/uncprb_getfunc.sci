// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function varargout = uncprb_getfunc(n,m,x,nprob,option)
  // Returns the function vector, the Jacobian and, if available, the Hessian.
  //
  // Calling Sequence
  //   fvec=uncprb_getfunc(n,m,x,nprob,option)
  //   [fvec,J]=uncprb_getfunc(n,m,x,nprob,option)
  //   [fvec,J,H]=uncprb_getfunc(n,m,x,nprob,option)
  //
  // Parameters
  //   n: the number of variables, i.e. the size of x
  //   m: the number of functions, i.e. the size of fvec
  //   x: a n x 1 matrix of doubles, the point where to compute f
  //   nprob: the problem number
  //   option: option=1 set fvec only (J=[]), option=2 set J only (fvec=[]), option= 3 set fvec and J, option= 4 set fvec, J and H
  //   fvec: a m x 1 matrix of doubles, the vector (f1(x), f2(x), ... fm(x))^T
  //   J: a m x n matrix of doubles, the Jacobian matrix, dfi(x)/dxj, i=1,..,m  , j=1, ..., n
  //   H: a n x n matrix of doubles, the Hessian matrix, d^2f(x)/dxi/dxj, i,j=1,..,n
  //
  // Description
  // Selects the appropriate test function based on nprob.
  //
  // Examples
  //   // Get fvec and J at x0 for Rosenbrock's test case
  //   nprob = 1
  //   [n,m,x0]=uncprb_getinitf(nprob)
  //   option = 3
  //   [fvec,J]=uncprb_getfunc(n,m,x0,nprob,option)
  //
  //   // See what happens if option=1
  //   option = 1
  //   [fvec,J]=uncprb_getfunc(n,m,x0,nprob,option)
  //
  //   // See what happens if option=2
  //   option = 2
  //   [fvec,J]=uncprb_getfunc(n,m,x0,nprob,option)
  //
  //   // See what happens if option=4 (the Hessian is available for Rosenbrock's problem)
  //   option = 4
  //   [fvec,J,H]=uncprb_getfunc(n,m,x0,nprob,option)
  //
  //   // See the error generated if option=4 for a problem for which the Hessian is not provided:
  //   // the Hessian is NOT available for problem #2.
  //   nprob = 2
  //   [n,m,x0]=uncprb_getinitf(nprob)
  //   option = 4
  //   [fvec,J,H]=uncprb_getfunc(n,m,x0,nprob,option)
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //   Scilab port: 2000-2004, Benoit Hamelin, Jean-Pierre Dussault
  //   Matlab port: 1994, Chaya Gurwitz, Livia Klein, Madhu Lamba
  //   Fortran 77 Hessian : 1993, Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
  //   Fortran 77: 1981, More, Garbow, Hillstrom

  [lhs,rhs]=argn();
  if ( rhs <> 5 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 5 are expected."), "uncprb_getfunc", rhs);
    error(errmsg)
  end
  if ( lhs > 3 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of output arguments : %d provided while 3 are expected."), "uncprb_getfunc", lhs);
    error(errmsg)
  end
  //
  if ( size(x) <> [n 1] ) then
    errmsg = msprintf(gettext("%s: Unexpected shape for input argument #3 : [%d %d] provided while [%d %d] is expected."), "uncprb_getfunc", size(x,"r") , size(x,"c") , n , 1 );
    error(errmsg)
  end

  //
  name = uncprb_getname(nprob)
  if ( lhs == 1 ) then
    execstr("fvec = uncprb_" + name + "(n,m,x,option)")
    varargout(1) = fvec
  elseif ( lhs == 2 ) then
    execstr("[fvec,J] = uncprb_" + name + "(n,m,x,option)")
    varargout(1) = fvec
    varargout(2) = J
  elseif ( lhs == 3 ) then
    execstr("[fvec,J,H] = uncprb_" + name + "(n,m,x,option)")
    varargout(1) = fvec
    varargout(2) = J
    varargout(3) = H
  end
endfunction

