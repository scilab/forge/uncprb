// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_gauss(n,m,x,option)

    // ****************************************************
    // ****************************************************
    // function [fvec, J] =gauss(n,m,x,option)
    // Gaussian function [9]
    // Dimensions   n=3,  m=15
    // Function definition:
    //       f(x)= x(1) exp[-x(2)*[(t(i)-x(3)]^2 / 2]-y(i)
    //       where t(i) = (8-i)/2
    // Standard starting point at x=(0.4,1,0)
    // Minima of f=1.12793...10^(-8)
    //

    if ( option<1 | option>3 ) then
        msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_gauss",option);
    end
    //
    fvec=[]
    J=[]
    H = []
    //
    y = [0.0009,0.0044,0.0175,0.054,0.1295,0.242,0.3521,0.3989,0.3521,0.242,0.1295,0.054,0.0175,0.0044,0.0009]';

    if ( option==2 | option==3 | option==4 ) then
        J = zeros(m,n);
    end

    i = (1:m)'
    t = (8-i)/2
    //
    if ( option==1 | option==3 | option==4 ) then
        fvec = x(1)*exp((-x(2)*((t-x(3)).^2))/2)-y;
    end
    //
    if ( option==2 | option==3 | option==4 ) then
        J(i,1) = exp((-x(2)*((t(i)-x(3)).^2))/2);
        J(i,2) = x(1)*((-((t(i)-x(3)).^2))/2).*exp((-x(2)*((t(i)-x(3)).^2))/2);
        J(i,3) = x(1)*x(2)*(t(i)-x(3)).*exp((-x(2)*((t(i)-x(3)).^2))/2);
    end
    //
    if ( option==4 ) then
        hesd(1:3) = 0
        hesl(1:3) = 0
        for i = 1: 15
            d1 = 5.0D-1*(i-1)
            d2 = 3.5 - d1 - x(3)
            arg = 5.0D-1*x(2)*d2**2
            r = exp(-arg)
            t = x(1)*r - y(i)
            t1 = 2*x(1)*r - y(i)
            hesd(1) = hesd(1) + r**2
            hesd(2) = hesd(2) + r*t1*d2**4
            hesd(3) = hesd(3) + r*(x(2)*t1*d2**2-t)
            hesl(1) = hesl(1) - r*t1*d2**2
            hesl(2) = hesl(2) + d2*r*t1
            hesl(3) = hesl(3) + d2*r*(t-arg*t1)
        end
        hesd(1) = 2*hesd(1)
        hesd(2) = 5.0D-1*x(1)*hesd(2)
        hesd(3) = 2*x(1)*x(2)*hesd(3)
        hesl(2) = 2*x(2)*hesl(2)
        hesl(3) = 2*x(1)*hesl(3)
        //
        // Reconstitute the Hessian matrix
        H = uncprb_hesdl2H ( hesd , hesl )
    end
endfunction

function apifun_checktrue ( var , msg )
    if ( ~var ) then
        error(msg);
    end
endfunction

