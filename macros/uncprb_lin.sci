// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.
function [fvec,J]=uncprb_lin(n,m,x,option)

  //Function [fvec,J]= lin(n,m,x,option)
  //Linear function - full rank [32]
  //Dimensions: n variable,      m>=n
  //Standard starting point: (1,...,1)
  //Minima of f=m-n at (-1,...,-1)
  //
  //Coded in MATLAB   11/94        plk
  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_lin",option);
  end
  //
  apifun_checktrue ( m>=n , msprintf(gettext("%s: Expected m>=n, but got the contrary. m=%d, n=%d"), "uncprb_lin" , m , n ))
  //
  fvec=[]
  J=[]
  //
  if ( option==2 | option==3 ) then
    J = zeros(m,n);
  end
  for i = 1:n
    sum1 = sum(x);
    if ( option==1 | option==3 ) then
      fvec(i) = x(i)-2/m*sum1-1;
    end
    if ( option==2 | option==3 ) then
      for j = 1:n
        if i==j then
          J(i,j) = 1-2/m;
        else
          J(i,j) = -2/m;
        end
      end
    end
  end
  for i = n+1:m
    if ( option==1 | option==3 ) then
      fvec(i) = -2/m*sum1-1
    end
    if ( option==2 | option==3 ) then
      for j = 1:n
        J(i,j) = -2/m;
      end
    end
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

