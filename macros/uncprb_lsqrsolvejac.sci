// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function J = uncprb_lsqrsolvejac ( x, m, nprob, n )
    // Returns the Jacobian matrix for the lsqrsolve function.
    //
    // Calling Sequence
    //   J = uncprb_lsqrsolvejac ( x, m, nprob, n )
    //
    // Parameters
    //   x: a n-by-1 matrix of doubles, the point where to compute f and g
    //   nprob: a 1-by-1 matrix of doubles, integer value, the problem number
    //   n: a 1-by-1 matrix of doubles, integer value, the number of variables, i.e. the size of x
    //   m: a 1-by-1 matrix of doubles, integer value, the number of functions, i.e. the size of fvec
    //   J: a m-by-n matrix of doubles, the Jacobian matrix
    //
    // Description
    // This function is designed to be used to make the lsqrsolve solver
    // optimize the problem #nprob.
    // Indeed, the header of the uncprb_lsqrsolvefun function is the one required
    // by the optim function.
    //
    // Examples
    //   // Check wrapper on problem #1
    //   nprob = 1;
    //   [n,m,x0]=uncprb_getinitf(nprob);
    //   J = uncprb_lsqrsolvejac ( x0, m, nprob, n )
    //   //     24.    10.
    //   //  - 1.     0.
    // Authors
    //   Michael Baudin - 2011 - DIGITEO

    x = x(:)
    J = uncprb_getvecjac(n,m,x,nprob)
    global __LSQRSOLVEDATA__
    // Initialize the structure if not already done.
    if ( __LSQRSOLVEDATA__ == [] ) then
        uncprb_lsqrsolveinit()
    end
    __LSQRSOLVEDATA__.Jeval = __LSQRSOLVEDATA__.Jeval + 1
endfunction
