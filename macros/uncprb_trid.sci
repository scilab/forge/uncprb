// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_trid(n,m,x,option)

  // Broyden tridiagonal function
  // ----------------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=trid(n,m,x,option)
  // Dimensions -> n=variable, m=n
  // Problem no. 30
  // Standard starting point -> x=(-1,..,-1)
  // Minima -> f=0
  //
  // 11/21/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_trid",option);
  end
  //
  fvec=[];
  J=[];
  //
  if ( option==2 | option==3 ) then
    J = zeros(m,n);
  end
  for i = 1:n

    if ( option==1 | option==3 ) then

      x(n+1) = 0
      if i==1 then
        fvec(i) = (3-2*x(i))*x(i)-2*x(i+1)+1;
      elseif i==n then
        fvec(i) = (3-2*x(i))*x(i)-x(i-1)+1;
      else
        fvec(i) = (3-2*x(i))*x(i)-x(i-1)-2*x(i+1)+1;
      end
    end

    if ( option==2 | option==3 ) then
      J(i,i) = 3-4*x(i);
      if i<n then
        J(i,i+1) = -2;
        J(i+1,i) = -1;
      end
    end
  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

