// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_rosex(n,m,x,option)

  // Extended Rosenbrock function
  // ----------------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=rosex(n,m,x,option)
  // Dimensions -> n=variable but even, m=n
  // Problem no. 21
  // Standard starting point -> x=(s(j)) where
  //                            s(2*j-1)=-1.2,
  //                            s(2*j)=1
  // Minima -> f=0 at (1,.....,1)
  //
  // 11/21/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_rosex",option);
  end
  //
  apifun_checktrue ( m==n , msprintf(gettext("%s: Expected m==n, but got the contrary. m=%d, n=%d"), "uncprb_rosex" , m , n ))
  apifun_checktrue ( pmodulo(n,2)==0 , msprintf(gettext("%s: Expected pmodulo(n,2)==0, but got the contrary. n=%d"), "uncprb_rosex" , n ))
  //
  fvec=[];
  J=[];
  H=[]
  //
  if ( option==2 | option==3 ) then
    J = zeros(m,n);
  end

  for i = 1:m/2

    if ( option==1 | option==3 | option==4 ) then
      fvec(2*i-1) = 10*(x(2*i)-(x(2*i-1)^2));
      fvec(2*i) = 1-x(2*i-1)
    end

    if ( option==2 | option==3 | option==4 ) then
      J(2*i-1,2*i-1) = -20*x(2*i-1);
      J(2*i-1,2*i) = 10;
      J(2*i,2*i-1) = -1;
    end

  end
  //
  if ( option==4 ) then
    hesl(1: n*(n-1)/2) = 0
    for j = 1:2: n
      hesd(j+1) = 200
      hesd(j) = 1200*x(j)**2 - 400*x(j+1) + 2
      hesl(j*(j-1)/2+j) = -400*x(j)
    end
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

