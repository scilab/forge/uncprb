// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function funeval = uncprb_optimget()
    // Get the fields of the internal data for uncprb_optimfun.
    //
    // Calling Sequence
    //   funeval = uncprb_optimget()
    //
    // Parameters
    //   funeval: a 1-by-1 matrix of doubles, integer value, the number of function evaluations since the previous call to uncprb_optiminit()
    //
    // Description
    // Get the fields of the internal data maintained by uncprb_optimfun.
    //
    // The values returned by the uncprb_optimget function depends on the
    // number of calls to the uncprb_optimfun function.
    // When the uncprb_optimfun function is called, then funeval is increased.
    //
    // Examples
    //   // See uncprb_optimfun
    //
    // Authors
    //   Michael Baudin - 2011 - DIGITEO

    global __OPTIMFUNDATA__

    funeval = __OPTIMFUNDATA__.funeval
endfunction

