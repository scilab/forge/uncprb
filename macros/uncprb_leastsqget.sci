// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [funeval,Jeval] = uncprb_leastsqget()
    // Get the fields of the internal data for uncprb_leastsqfun.
    //
    // Calling Sequence
    //   [funeval,Jeval] = uncprb_leastsqget()
    //
    // Parameters
    //   funeval: a 1-by-1 matrix of doubles, integer value, the number of function evaluations since the previous call to uncprb_leastsqinit()
    //   Jeval: a 1-by-1 matrix of doubles, integer value, the number of Jacobian evaluations since the previous call to uncprb_leastsqinit()
    //
    // Description
    // Get the internal data structure used by uncprb_leastsqfun and
    // uncprb_leastsqjac.
    //
    // The values returned by the uncprb_leastsqget function depends on the
    // number of calls to the uncprb_leastsqfun and uncprb_leastsqjac
    // function.
    // <itemizedlist>
    // <listitem><para>
    // When the uncprb_leastsqfun function is called, then funeval is increased.
    // </para></listitem>
    // <listitem><para>
    // When the uncprb_leastsqjac function is called, then Jeval is increased.
    // </para></listitem>
    // </itemizedlist>
    //
    // Examples
    //   // See uncprb_leastsqfun
    //
    // Authors
    //   Michael Baudin - 2011 - DIGITEO

    global __LEASTSQDATA__

    funeval = __LEASTSQDATA__.funeval
    Jeval = __LEASTSQDATA__.Jeval
endfunction

