// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function r = uncprb_leastsqfun ( x, m, nprob, n )
    // Returns the residual vector for the leastsq function.
    //
    // Calling Sequence
    //   r = uncprb_leastsqfun ( x, m, nprob, n )
    //
    // Parameters
    //   x: a n-by-1 matrix of doubles, the point where to compute f and g
    //   nprob: a 1-by-1 matrix of doubles, integer value, the problem number
    //   n: a 1-by-1 matrix of doubles, integer value, the number of variables, i.e. the size of x
    //   m: a 1-by-1 matrix of doubles, integer value, the number of functions, i.e. the size of fvec
    //   r: a m-by-1 matrix of doubles, the residual
    //
    // Description
    // This function is designed to be used to make the leastsq solver
    // optimize the problem #nprob.
    // Indeed, the header of the uncprb_leastsqfun function is the one required
    // by the leastsq function.
    //
    // Examples
    //   // Check wrapper on problem #1
    //   nprob = 1;
    //   [n,m,x0]=uncprb_getinitf(nprob);
    //   r = uncprb_leastsqfun ( x0, m, nprob, n )
    //   // r0 = [-4.4; 2.2]
    //
    //   // Make leastsq optimize the problem #1
    //   nprob = 1;
    //   [n,m,x0]=uncprb_getinitf(nprob)
	//   objfun = list(uncprb_leastsqfun, m, nprob, n);
    //   [fopt,xopt,gopt]=leastsq(objfun,x0)
    //
    //   // Make leastsq optimize the problem #1
    //   // Get the number of iterations, function and gradient evaluations
    //   nprob = 1;
    //   [n,m,x0]=uncprb_getinitf(nprob);
    //   uncprb_leastsqinit();
    //   objfun = list(uncprb_leastsqfun, m, nprob, n);
    //   [fopt,xopt,gopt]=leastsq(objfun,x0)
    //   [foptE,xoptE] = uncprb_getopt(nprob,n,m)
    //   goptE = uncprb_getgrdfcn(n,m,xoptE,nprob)
    //   [funeval,Jeval] = uncprb_leastsqget()
    //
    //   // Make lsqrsolve optimize the problem #1
    //   // With Jacobian
    //   // Get the number of iterations, function and gradient evaluations
    //   nprob = 1;
    //   [n,m,x0]=uncprb_getinitf(nprob);
    //   uncprb_leastsqinit();
    //   objfun = list(uncprb_leastsqfun, m, nprob, n);
    //   [fopt,xopt,gopt]=leastsq(objfun,uncprb_leastsqjac,x0)
    //   [foptE,xoptE] = uncprb_getopt(nprob,n,m)
    //   goptE = uncprb_getgrdfcn(n,m,xoptE,nprob)
    //   [funeval,Jeval] = uncprb_leastsqget()
    //
    // Authors
    //   Michael Baudin - 2011 - DIGITEO

    x = x(:)
    r = uncprb_getvecfcn(n,m,x,nprob)
    global __LEASTSQDATA__

    // Initialize the structure if not already done.
    if ( __LEASTSQDATA__ == [] ) then
        uncprb_leastsqinit()
    end
    __LEASTSQDATA__.funeval = __LEASTSQDATA__.funeval + 1
endfunction
