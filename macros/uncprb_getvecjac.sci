// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function J=uncprb_getvecjac(n,m,x,nprob)
  // Returns the Jacobian.
  //
  // Calling Sequence
  //   J=uncprb_getvecjac(n,m,x,nprob)
  //
  // Parameters
  //   n: a floating point integer, the number of variables, i.e. the size of x
  //   m: a floating point integer, the number of functions, i.e. the size of fvec
  //   x: a n x 1 matrix of doubles, the point where to compute J
  //   nprob: a floating point integer, the problem number
  //   J: a m x n matrix of doubles, the Jacobian matrix, dfi(x)/dxj, i=1,..,m  , j=1, ..., n
  //
  // Description
  // It is an interface function which calls the function uncprb_getfunc (which selects
  // appropriate test function based on nprob) with option=2 in order to return
  // J, the vector.
  //
  // Examples
  //   // Get Jacobian at x0 for Rosenbrock's test case
  //   nprob = 1
  //   [n,m,x0]=uncprb_getinitf(nprob)
  //   J = uncprb_getvecjac(n,m,x0,nprob)
  //
  //   // Get Jacobian at x* for Rosenbrock's test case
  //   [fopt,xopt] = uncprb_getopt(nprob,n,m)
  //   Jopt = uncprb_getvecjac(n,m,xopt,nprob)
  //
  // Authors
  //   Michael Baudin - 2010 - DIGITEO
  //   Scilab port: 2000-2004, Benoit Hamelin, Jean-Pierre Dussault
  //   Matlab port: 1994, Chaya Gurwitz, Livia Klein, Madhu Lamba
  //   Fortran 77: 1981, More, Garbow, Hillstrom

  [lhs,rhs]=argn();
  if ( rhs <> 4 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 4 are expected."), "uncprb_getvecjac", rhs);
    error(errmsg)
  end
  //
  [fvec,J] = uncprb_getfunc(n,m,x,nprob,2)
endfunction

