// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_cheb(n,m,x,option)

  //  function [fvec,J] = uncprb_cheb(n,m,x,option)
  // Problem no. 35
  // CHEBYQUAD FUNCTION [11]
  // (a) n variable, m>=n
  // (b) ...
  // (c) x0 = (xi_j) where xi_j = j/(n+1)
  // (d) f = 0 for m=n, 1<= n<= 7 and n = 9
  //     f = 3.51687...e-3 for m=m=8
  //     f = 6.50395...e-3 for m=n=10
  //
  //  MB, added
  //

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_bd",option);
  end
  //
  apifun_checktrue ( m>=n , msprintf(gettext("%s: Expected m>=n, but got the contrary. m=%d, n=%d"), "uncprb_cheb" , m , n ))
  //
  fvec=[]
  J=[]
  H=[]
  //
  if ( option==1 | option==3 | option==4 ) then
    fvec = zeros(n,1)
    for j = 1:n
      temp1 = 1
      temp2 = 2*x(j) - 1
      temp = 2*temp2
      for i = 1:n
        fvec(i) = fvec(i) + temp2
        ti = temp*temp2 - temp1
        temp1 = temp2
        temp2 = ti
      end
    end
    tk = 1/n
    iev = -1
    for k = 1:n
      fvec(k) = tk*fvec(k)
      if (iev > 0) then
        fvec(k) = fvec(k) + 1/(k**2 - 1)
      end
      iev = -iev
    end
  end
  //
  if ( option==2 | option==3 | option==4 ) then
    tk = 1/n
    for j = 1:n
      temp1 = 1
      temp2 = 2*x(j) - 1
      temp = 2*temp2
      temp3 = 0
      temp4 = 2
      for k = 1:n
        J(k,j) = tk*temp4
        ti = 4*temp2 + temp*temp4 - temp3
        temp3 = temp4
        temp4 = ti
        ti = temp*temp2 - temp1
        temp1 = temp2
        temp2 = ti
      end
    end
  end
  //
  if ( option==4 ) then
    d1 = 1/n
    d2 = 2*d1
    im = 0
    for j = 1: n
      hesd(j) = 4*d1
      t1 = 1
      t2 = 2*x(j) - 1
      t = 2*t2
      s1 = 0
      s2 = 2
      p1 = 0
      p2 = 0
      gvec(1) = s2
      for i = 2: n
        th = 4*t2 + t*s2 - s1
        s1 = s2
        s2 = th
        th = t*t2 - t1
        t1 = t2
        t2 = th
        th = 8*s1 + t*p2 - p1
        p1 = p2
        p2 = th
        gvec(i) = s2
        hesd(j) = hesd(j) + fvec(i)*th + d1*s2**2
      end
      hesd(j) = d2*hesd(j)
      for k = 1: j-1
        im = im + 1
        hesl(im) = 0
        tt1 = 1
        tt2 = 2*x(k) - 1
        tt = 2*tt2
        ss1 = 0
        ss2 = 2
        for i = 1: n
          hesl(im) = hesl(im) + ss2*gvec(i)
          tth = 4*tt2 + tt*ss2 - ss1
          ss1 = ss2
          ss2 = tth
          tth = tt*tt2 - tt1
          tt1 = tt2
          tt2 = tth
        end
        hesl(im) = d2*d1*hesl(im)
      end
    end
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

