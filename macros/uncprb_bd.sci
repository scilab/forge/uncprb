// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_bd(n,m,x,option)

  //  function [fvec,J] = bd(n,m,x,option)
  //  Brown and Dennis function  [16]
  //  Dimensions:  n=4,  m=20
  //  Function Definition:
  //       f(x)=(x1 + t(i)x2- exp[t(i)])^2 +(x3 + x4sin(t(i))- cos(t(i)))^2
  //       where t(i)=i/5
  //  Standard starting point (25,5,-5,-1)
  //  Minima of f=85822.2... if m=20
  //
  //  Revised  11/94               PLK

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_bd",option);
  end
  //
  apifun_checktrue ( m>=n , msprintf(gettext("%s: Expected m>=n, but got the contrary. m=%d, n=%d"), "uncprb_bd" , m , n ))
  //
  fvec=[]
  J=[]
  H=[]
  //
  x1 = x(1);
  x2 = x(2);
  x3 = x(3);
  x4 = x(4);
  //
  t = (1:m)'*0.2;
  e = exp(t);
  s = sin(t);
  c = cos(t);
  //
  if ( option==1 | option==3 | option==4 ) then
    for i = 1:m
      fvec(i) = (x1+t(i)*x2-e(i))^2+(x3+x4*s(i)-c(i))^2;
    end
  end
  //
  if ( option==2 | option==3 | option==4 ) then
    for i = 1:m
      f1 = 2*(x1+t(i)*x2-e(i));
      f3 = 2*(x3+x4*s(i)-c(i));
      J(i,1) = f1;
      J(i,2) = f1*t(i);
      J(i,3) = f3;
      J(i,4) = f3*s(i);
    end
  end
  //
  if ( option==4 ) then
    hesd(1: 4) = 0
    hesl(1: 6) = 0
    for i = 1: 20
      t1 = x(1) + t(i)*x(2) - e(i)
      t2 = x(3) + s(i)*x(4) - c(i)
      tt = 8 * t1 * t2
      s1 = 12*t1**2 + 4*t2**2
      s2 = 12*t2**2 + 4*t1**2
      hesd(1) = hesd(1) + s1
      hesd(2) = hesd(2) + s1*t(i)**2
      hesd(3) = hesd(3) + s2
      hesd(4) = hesd(4) + s2*s(i)**2
      hesl(1) = hesl(1) + s1*t(i)
      hesl(2) = hesl(2) + tt
      hesl(4) = hesl(4) + tt*s(i)
      hesl(3) = hesl(3) + tt*t(i)
      hesl(5) = hesl(5) + tt*t(i)*s(i)
      hesl(6) = hesl(6) + s2*s(i)
    end
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

