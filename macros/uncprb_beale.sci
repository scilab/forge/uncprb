// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_beale(n,m,x,option)

  // Beale function
  // --------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=beale(n,m,x,option)
  // Problem no. 5
  // Dimensions -> n=2, m=3
  // Standard starting point -> x=(1,1)
  // Minima -> f=0 at (3,0.5)
  //
  // Revised on 10/22/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_beale",option);
  end
  //
  fvec=[]
  J=[]
  H=[]
  //
  if ( option==1 | option==3 | option==4 ) then
    fvec = [1.5-x(1)*(1-x(2));2.25-x(1)*(1-(x(2)^2));2.625-x(1)*(1-(x(2)^3))];
  end
  //
  if ( option==2 | option==3 | option==4 ) then
    J = [-(1-x(2)),x(1);-(1-(x(2)^2)),x(1)*2*x(2);-(1-(x(2)^3)),x(1)*3*(x(2)^2)];
  end
  //
  if ( option==4 ) then
    s1 = 1 - x(2)
    t1 = 1.5 - x(1)*s1
    s2 = 1 - x(2)**2
    t2 = 2.25 - x(1)*s2
    s3 = 1 - x(2)**3
    t3 = 2.625 - x(1)*s3
    hesd(1) = 2 * (s1**2 + s2**2 + s3**2)
    hesd(2) = 2*x(1) * (x(1) + 2*t2 + 4*x(1)*x(2)**2 + 6*x(2)*t3 + 9*x(1)*x(2)**4)
    hesl(1) = 2*(t1-x(1)*s1) + 4*x(2)*(t2-x(1)*s2) + 6*(t3-x(1)*s3)*x(2)**2
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

