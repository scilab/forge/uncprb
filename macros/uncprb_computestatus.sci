// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [statusF, statusX] = uncprb_computestatus ( nprob, xopt, fopt, rtolF, atolF, rtolX, atolX )
    // Computes the status of an optimization.
    //
    // Calling Sequence
    //   [statusF, statusX] = uncprb_computestatus ( nprob, xopt, fopt, tolF, tolX )
    //
    // Parameters
    //   nprob: a 1-by-1 matrix of doubles, integer value, the problem number
    //   xopt: a n-by-1 matrix of doubles, the computed optimum X
    //   fopt: a 1-by-1 matrix of doubles, the computed optimum F(X)
    //   rtolF: a 1-by-1 matrix of doubles, positive, a relative tolerance on F
    //   atolF: a 1-by-1 matrix of doubles, positive, an absolute tolerance on F
    //   rtolX: a 1-by-1 matrix of doubles, positive, a relative tolerance on X
    //   atolX: a 1-by-1 matrix of doubles, positive, an absolute tolerance on X
	//   statusF : a 1-by-1 matrix of booleans. statusF is %t if the convergence condition on F is achieved
	//   statusX : a 1-by-1 matrix of booleans. statusX is %t if the convergence condition on X is achieved
    //
    // Description
    // Returns the status of the optimization with respect to
    // the function value or the point X.
	//
	// The xstar input argument may be equal to the empty matrix [], if the
	// expected optimum X is unknown for this case.
	// This depends on the output of the uncprb_getopt function.
	// In this case, statusX is "OK".
	//
	// The status on F is "OK" if the following condition is satisfied:
	//
	// <programlisting>
	// abs(fopt-fstar)< rtolF*abs(fstar) + atolF
	// </programlisting>
	//
	// The status on X is "OK" if xstar==[] or if the following condition is satisfied:
	//
	// <programlisting>
	// norm(xopt-xstar)< rtolX*norm(xstar) + atolX
	// </programlisting>
    //
    // Examples
    //   // Make optim optimize the problem #4
    //   nprob = 4;
    //   [n,m,x0]=uncprb_getinitf(nprob)
    //   [fopt,xopt,gopt]=optim(list(uncprb_optimfun,nprob,n,m),x0,"gc")
	//   rtolX = 1.e-4;
	//   atolX = 1.e-10;
	//   rtolF = 1.e-4;
	//   atolF = 1.e-10;
	//   [statusF, statusX] = uncprb_computestatus ( nprob, xopt, fopt, ..
	//        rtolF, atolF, rtolX, atolX )
    //
    // Authors
    //   Michael Baudin - 2011 - DIGITEO

    [fstar,xstar] = uncprb_getopt(nprob,n,m)
    statusF = ( abs(fopt-fstar)< rtolF*abs(fstar) + atolF )
    if ( xstar==[] ) then
        statusX = %t
    else
        statusX = ( norm(xopt-xstar)< rtolX*norm(xstar) + atolX )
    end
endfunction
