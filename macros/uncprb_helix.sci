// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_helix(n,m,x,option)


    // *******************************************
    // *******************************************
    // function [ fvec, J]= helix(n,m,x,option)
    //
    // Helical valley function  [7]
    // Dimensions    n=3,   m=3
    // Function Definition:
    //       f1(x) = 10[x3 - 10*(x1,x2)]
    //       f2(x) = 10[((x1)^2 + (x2)^2)^.5 -1]
    //       f3(x) = x3
    // Standard starting point  x= (-1,0,0)
    // Minima of f=0 at (1,0,0)
    //

    if ( option<1 | option>4 ) then
        msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_helix",option);
    end
    //
    fvec=[]
    J=[]
    H=[]
    //
    if ( option==1 | option==3 | option==4 ) then
        if x(1)>0 then
            fvec(1) = 10*(x(3)-10*(1/(2*%pi)*atan(x(2)/x(1))));
        elseif x(1)<0 then
            fvec(1) = 10*(x(3)-10*(1/(2*%pi)*atan(x(2)/x(1))+0.5));
        else
            fvec(1) = 0
        end

        fvec(2) = 10*((x(1)^2+x(2)^2)^0.5-1)
        fvec(3)=x(3)
    end

    if ( option==2 | option==3 | option==4 ) then
        J(1,1) = 50/%pi*(x(2)/(x(1)^2))*(1/(1+x(2)/x(1)^2));
        J(1,2) = (-50/%pi)*(1/x(1))*(1/(1+x(2)/x(1)^2));
        J(1,3) = 10;

        J(2,1) = 10*x(1)/sqrt(x(1)^2+x(2)^2);
        J(2,2) = 10*x(2)/sqrt(x(1)^2+x(2)^2);
        J(2,3) = 0;

        J(3,1) = 0;
        J(3,2) = 0;
        J(3,3) = 1;
    end

    if ( option == 4 ) then
        if (x(1) == 0) then
            th = sign(2.5D-1,x(2))
        else
            th = atan(x(2)/x(1)) / (2*%pi)
            if (x(1) < 0) then
                th = th + 5.0D-1
            end
        end
        arg = x(1)**2 + x(2)**2
        piarg = %pi * arg
        piarg2 = piarg * arg
        r3inv = 1 / sqrt(arg)**3
        t = x(3) - 10*th
        s1 = 5*t / piarg
        p1 = 2.0D3*x(1)*x(2)*t / piarg2
        p2 = (5/piarg)**2
        hesd(1) = 2.0D2 - 2.0D2*(r3inv-p2)*x(2)**2 - p1
        hesd(2) = 2.0D2 - 2.0D2*(r3inv-p2)*x(1)**2 + p1
        hesd(3) = 202
        hesl(1) = 2.0D2*x(1)*x(2)*r3inv + 1.0D3/piarg2 * ( t*(x(1)**2-x(2)**2) - 5*x(1)*x(2)/%pi )
        hesl(2) =  1.0D3*x(2) / piarg
        hesl(3) = -1.0D3*x(1) / piarg
        //
        // Reconstitute the Hessian matrix
        H = uncprb_hesdl2H ( hesd , hesl )
    end
endfunction
function apifun_checktrue ( var , msg )
    if ( ~var ) then
        error(msg);
    end
endfunction


