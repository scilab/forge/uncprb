// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J,H]=uncprb_badscp(n,m,x,option)

  // Powell badley scaled function
  // -----------------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=badscp(n,m,x,option)
  // Problem no. 3
  // Dimensions -> n=2, m=2
  // Standard starting point -> x=(0,1)
  // Minima -> f=0 at (1.098...10-E5,9.106...)
  //
  // Revised on 10/22/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ( option<1 | option>4 ) then
    msprintf("%s: Error: The option value is %d, while either 1 to 4 are expected." , "uncprb_osb2",option);
  end
  //
  fvec=[]
  J=[]
  H = []
  //
  if ( option==1 | option==3 | option==4 ) then
    fvec = [(10^4)*x(1)*x(2)-1;exp(-x(1))+exp(-x(2))-1.0001];
  end
  if ( option==2 | option==3 | option==4 ) then
    J = [(10^4)*x(2),(10^4)*x(1);-exp(-x(1)),-exp(-x(2))];
  end
  //
  if ( option==4 ) then
    s1 = exp(-x(1))
    s2 = exp(-x(2))
    t2 = s1 + s2 - 1 - 0.0001
    hesd(1) = 2.e8*x(2)**2 + 2*s1*(s1+t2)
    hesd(2) = 2.e8*x(1)**2 + 2*s2*(s2+t2)
    hesl(1) = 4.e8*x(1)*x(2) + 2*s1*s2 - 20000
    //
    // Reconstitute the Hessian matrix
    H = uncprb_hesdl2H ( hesd , hesl )
  end
endfunction

function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

