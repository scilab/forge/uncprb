// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [f,g,ind]=uncprb_optimfun(x,ind,nprob,n,m)
    // Returns f and g for the optim function.
    //
    // Calling Sequence
    //   [f,g,ind]=uncprb_optimfun(x,ind,nprob,n,m)
    //
    // Parameters
    //   x: a n-by-1 matrix of doubles, the point where to compute f and g
    //   ind: a n-by-1 matrix of doubles, integer value, a flag indicating what to compute or print
    //   nprob: a 1-by-1 matrix of doubles, integer value, the problem number
    //   n: a 1-by-1 matrix of doubles, integer value, the number of variables, i.e. the size of x
    //   m: a 1-by-1 matrix of doubles, integer value, the number of functions, i.e. the size of fvec
    //   f: a 1-by-1 matrix of doubles, the function value
    //   g: a n-by-1 matrix of doubles, the gradient
    //
    // Description
    // This function is designed to be used to make the optim solver
    // optimize the problem #nprob.
    // Indeed, the header of the uncprb_optimfun function is the one required
    // by the optim function.
    //
    // Examples
    //   // Check wrapper on problem #1
    //   nprob = 1;
    //   [n,m,x0]=uncprb_getinitf(nprob);
    //   [f0,g0,ind]=uncprb_optimfun(x0,4,nprob,n,m);
    //   // f0 : 24.2
    //   // g0 : [-215.6 -88]
    //
    //   // Make optim optimize the problem #1
    //   nprob = 1;
    //   [n,m,x0]=uncprb_getinitf(nprob)
    //   [fopt,xopt,gopt]=optim(list(uncprb_optimfun,nprob,n,m),x0,"gc")
	//
	//   // Make optim optimize the problem #1
	//   // Get the number of iterations, function and gradient evaluations
	//   nprob = 1;
	//   [n,m,x0]=uncprb_getinitf(nprob);
	//   uncprb_optiminit();
	//   [fopt,xopt,gopt]=optim(list(uncprb_optimfun,nprob,n,m),x0)
	//   [foptE,xoptE] = uncprb_getopt(nprob,n,m)
	//   goptE = uncprb_getgrdfcn(n,m,xoptE,nprob)
	//   funeval = uncprb_optimget()
    //
    // Authors
    //   Michael Baudin - 2011 - DIGITEO

    global __OPTIMFUNDATA__

    // Initialize the structure if not already done.
    if ( __OPTIMFUNDATA__ == [] ) then
        uncprb_optiminit()
    end
    __OPTIMFUNDATA__.funeval = __OPTIMFUNDATA__.funeval + 1
    [n,m,x0] = uncprb_getinitf(nprob)
    x = x(:)
    f = uncprb_getobjfcn(n,m,x,nprob)
    g = uncprb_getgrdfcn(n,m,x,nprob)
endfunction

