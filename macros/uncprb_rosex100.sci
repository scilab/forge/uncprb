// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1994 - Chaya Gurwitz, Livia Klein, Madhu Lamba
// Copyright (C) 1981 - More, Garbow, Hillstrom
//
// This file must be used under the terms of the GNU LGPL license.

function [fvec,J]=uncprb_rosex(n,m,x,option)

  // Extended Rosenbrock function
  // ----------------------------
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // function [fvec,J]=rosex(n,m,x,option)
  // Dimensions -> n=variable but even, m=n
  // Problem no. 21
  // Standard starting point -> x=(s(j)) where
  //                            s(2*j-1)=-1.2,
  //                            s(2*j)=1
  // Minima -> f=0 at (1,.....,1)
  //
  // 11/21/94 by Madhu Lamba
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  if ( option<1 | option>3 ) then
    msprintf("%s: Error: The option value is %d, while either 1,2 or 3 are expected." , "uncprb_rosex",option);
  end
  //
  fvec=[];
  J=[];
  //
  if ( option==2 | option==3 ) then
    J = zeros(m,n);
  end

  for i = 1:m/2

    if ( option==1 | option==3 ) then
      fvec(2*i-1) = 10*(x(2*i)-(x(2*i-1)^2));
      fvec(2*i) = 1-x(2*i-1)
    end

    if ( option==2 | option==3 ) then
      J(2*i-1,2*i-1) = -20*x(2*i-1);
      J(2*i-1,2*i) = 10;
      J(2*i,2*i-1) = -1;
    end

  end
endfunction
function apifun_checktrue ( var , msg )
  if ( ~var ) then
    error(msg);
  end
endfunction

