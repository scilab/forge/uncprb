// Copyright (C) 2010-2011 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

////////////////////////////////////////

mprintf("\n==\n");
mprintf("Testing leastsq on several problems in this collection.\n");

//
// Test leastsq on these problems
// Tolerances on function value
rtolF = 1.e-4;
atolF = 1.e-10;
rtolX = 1.e-4;
atolX = 1.e-6;
msgfmt = "wiki";
mprintf("==================================\n")
mprintf("Compares several approaches with leastsq:\n")
mprintf(" * leastsq without Jacobian\n")
mprintf(" * leastsq with exact Jacobian\n")
mprintf("==================================\n")
mprintf("Relative Tolerance on F:%e\n",rtolF)
mprintf("Relative Tolerance on X:%e\n",rtolX)
mprintf("Absolute Tolerance on F:%e\n",atolF)
mprintf("Absolute Tolerance on X:%e\n",atolX)
mprintf("==================================\n")


//
// Testing leastsq without Jacobian
mprintf("Testing leastsq without Jacobian\n")
nprobmax = uncprb_getproblems();
nsuccess = 0;
funevalTotal = 0;
iter = [];
geval = [];
tic();
success = uncprb_printsummary("leastsq", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    rtolF, atolF, rtolX, atolX, "wikihead");
for nprob = 1 : nprobmax
    [n,m,x0] = uncprb_getinitf(nprob);
    uncprb_leastsqinit();
    objfun = list(uncprb_leastsqfun, m, nprob, n);
    [fopt,xopt,gopt]=leastsq(objfun,x0);
    funeval = uncprb_leastsqget();
    funevalTotal = funevalTotal + funeval;
    success = uncprb_printsummary("leastsq", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    rtolF, atolF, rtolX, atolX, msgfmt);
    if ( success ) then
        nsuccess = nsuccess + 1;
    end
end
t = toc();
mprintf("Time: %d (s)\n",t);
mprintf("Total number of problems:%d\n",nprobmax);
mprintf("Number of successes:%d\n",nsuccess);
mprintf("Number of failures:%d\n",nprobmax-nsuccess);
mprintf("Total Feval:%d\n",funevalTotal);

//
// Testing leastsq with Jacobian
mprintf("Testing leastsq with Jacobian\n")
nprobmax = uncprb_getproblems();
nsuccess = 0;
funevalTotal = 0;
JevalTotal = 0;
iter = [];
tic();
success = uncprb_printsummary("leastsq", nprob, fopt, xopt, gopt, iter, funeval, geval, ..
    rtolF, atolF, rtolX, atolX, "wikihead");
for nprob = 1 : nprobmax
    [n,m,x0] = uncprb_getinitf(nprob);
    uncprb_leastsqinit();
    objfun = list(uncprb_leastsqfun, m, nprob, n);
    [fopt,xopt,gopt]=leastsq(objfun,uncprb_leastsqjac,x0);
    [funeval, Jeval] = uncprb_leastsqget();
    funevalTotal = funevalTotal + funeval;
    JevalTotal = JevalTotal + funeval;
    success = uncprb_printsummary("leastsq", nprob, fopt, xopt, gopt, iter, funeval, Jeval, ..
    rtolF, atolF, rtolX, atolX, msgfmt);
    if ( success ) then
        nsuccess = nsuccess + 1;
    end
end
t = toc();
mprintf("Time: %d (s)\n",t);
mprintf("Total number of problems:%d\n",nprobmax);
mprintf("Number of successes:%d\n",nsuccess);
mprintf("Number of failures:%d\n",nprobmax-nsuccess);
mprintf("Total Feval:%d\n",funevalTotal);
mprintf("Total Jeval:%d\n",JevalTotal);

