// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


////////////////////////////////////////

//
mprintf("\n==\n");
mprintf("What is the CPU cost of evaluating the objective function ?\n");
mprintf("Compare performances of the test problems.\n");
perf = [];
N = 100;
mprintf("Each problem is called %d times with option = 3.\n",N);
mprintf("Press return to continue\n");
halt()
mprintf("\n==\n");
nprobmax = uncprb_getproblems();
for nprob = 1 : nprobmax
  mprintf("Running problem %d.\n",nprob);
  [n,m,x0]=uncprb_getinitf(nprob);
  timer();
  for tt = 1 : 100
    [fvec,J]=uncprb_getfunc(n,m,x0,nprob,3);
  end
  perf(nprob) = timer();
end
perf = perf / N;
// Sort the performances by decreasing order
mprintf("\n");
mprintf("Sort the performances by decreasing order: Lower is better.\n");
mprintf("Press return to continue\n");
halt()
mprintf("\n==\n");
[B,k] = gsort(perf);
probs = 1:nprobmax;
namemat = uncprb_getname(1:nprobmax);
namemat = convstr(namemat,"u");
mprintf("nprob   Name       Time/call (s)\n")
mprintf("-----   ----       -------------\n")
for i = k'
  mprintf("%-5d   %-10s %-10f\n", probs(i), namemat(i), perf(i))
end


//
// Load this script into the editor
//
filename = "bench_problems.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );

