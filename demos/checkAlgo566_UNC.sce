// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

mprintf("\n==\n");
mprintf("Check the results of Scilab implementation\n")
mprintf("and compares against the Algorithm 566 / Fortran 77 results.\n")
mprintf("Copy the content of the console into output_UNC_Scilab.txt.\n")
mprintf("Compare with output_UNC_Algo566.txt, provided in this directory.\n")
mprintf("Compute X0, F(X0), G(X0) and H(X0) for problems for which the Hessian is provided.\n")
mprintf("There are 18 problems in this class.\n")
mprintf("Compare with the result of the Fortran 77 / Algo 566 / TESTH routine.\n")
mprintf("The Fortran routine tests the unconstrained optimization problems:\n")
mprintf("HELIX, BIGGS, GAUSS, BADSCP, BOX, VARDIM, WATSON, PEN1, PEN2, BADSCB, BD, GULF\n")
mprintf("TRIG, ROSEX, SINGX, BEALE, WOOD, CHEB\n")
mprintf("Notice that the problem numbers in Algo 566 are not the same as in the Scilab implementation\n")
mprintf("(but the names are the same).\n")
mprintf("Press return to continue\n");
halt()
mprintf("\n==\n");

// Copy the output of Scilab into output_UNC_Scilab.txt and compare with output_UNC_Algo566.txt.

for nprob = [7 18 9 3 12 25 20 23 24 4 16 11 26 21 22 5 14 35]
  mprintf("==========================\n")
  name=uncprb_getname(nprob);
  mprintf("Testing problem #%3d : %s\n",nprob,name)
  //
  [n,m,x0]=uncprb_getinitf(nprob);
  mprintf("N: %3d\n",n);
  mprintf("X0:\n")
  for i = 1 : n
    mprintf("X0(%3d)=%30.13f\n",i,x0(i));
  end
  //
  f0 = uncprb_getobjfcn(n,m,x0,nprob);
  mprintf("F0: %30.13f\n",f0);
  g0 = uncprb_getgrdfcn(n,m,x0,nprob);
  mprintf("G0:\n");
  for i = 1 : n
    mprintf("G0(%3d)=%30.13f\n",i,g0(i));
  end
  //
  H0 = uncprb_gethesfcn(n,m,x0,nprob);
  [hesd , hesl] = uncprb_H2hesdl ( H0 );
  mprintf("H0D:\n");
  for i = 1 : n
    mprintf("H0D(%3d)=%30.13f\n",i,hesd(i));
  end
  mprintf("H0L:\n");
  for i = 1 : n*(n-1)/2
    mprintf("H0L(%3d)=%30.13f\n",i,hesl(i));
  end
end

// The results are exactly the same.

//
// Load this script into the editor
//
filename = "checkAlgo566_UNC.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );

