// Copyright (C) 2010 - DIGITEO - Michael Baudin
// Copyright (C) 2000-2004 - Benoit Hamelin, Jean-Pierre Dussault
// Copyright (C) 1993 - Victoria Z. Averbukh, Samuel A. Figueroa, Tamar Schlick
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


mprintf("\n==\n");
mprintf("Check the results of Scilab implementation\n")
mprintf("and compares against the Algorithm 566 / Fortran 77 results.\n")
mprintf("Copy the content of the console into output_NLEQ_Scilab.txt.\n")
mprintf("Compare with output_NLEQ_Algo566.txt, provided in this directory.\n")
mprintf("Compute X0, F(X0), G(X0) and H(X0) for Non Linear Equations problems.\n")
mprintf("There are 14 problems in this class.\n")
mprintf("Compare with the result of the Fortran 77 / Algo 566 routines associated with Non Linear Equations :\n")
mprintf("ROSE, SING, BADSCP, WOOD, HELIX, WATSON, CHEB, ALMOST, BV, IE, TRIG, VARDIM, TRID, BAND\n")
mprintf("The dimensions are n = 2  4  2  4  3  9 10 10  8  8 10 10 12 15.\n")
mprintf("The number of functions are m = 2  4  2  6  3 31 10 10  8  8 10 12 12 15.\n")
mprintf("Notice that the problem numbers in Algo 566 are not the same as in the Scilab implementation\n")
mprintf("(but the names are the same).\n")
mprintf("Press return to continue\n");
halt()
mprintf("\n==\n");

nprobmax = uncprb_getproblems();
namemat = uncprb_getname(1:nprobmax);

for name = ["rose" "sing" "badscp" "wood" "helix" "watson" "cheb" "almost" "bv" "ie" "trig" "vardim" "trid" "band"]
  mprintf("==========================\n")
  nprob = find(namemat==name);
  mprintf("Testing problem #%3d : %s\n",nprob,name)
  //
  [n,m,x0]=uncprb_getinitf(nprob);
  mprintf("N: %3d\n",n);
  mprintf("M: %3d\n",m);
  mprintf("X0:\n")
  for i = 1 : n
    mprintf("X0(%3d)=%30.13f\n",i,x0(i));
  end
  //
  fvec=uncprb_getvecfcn(n,m,x0,nprob);
  mprintf("FVEC:\n");
  for i = 1 : m
    mprintf("FVEC(%3d)=%30.13f\n",i,fvec(i));
  end
end

// This leads to several comments.
//
// Problem #1 : Rosenbrock
//   The function values of fvec are switched : fvec(1) <-> fvec(2).
//   The Algo 566/Fortran implementation is not consistent with respect
//   to the order of the function values.
//   In particular, the values in SSQFCN and in VECFCN are switched.
//   The Scilab implementation is consistent with Algo 566/paper.
//
// Problem #14: Wood
//   The values of fvec are different.
//   In Scilab and in the Algo 566/paper, the function vector has 6 components.
//   In the Algo 566/Fortran implementation, the function vector has 4 components:
//   some components are summed.
//   The Scilab implementation is consistent with Algo 566/paper.
//
// Problem #20: Watson
//   In Scilab, the problem has m=31 components.
//   In Algo 566/Fortran, the problem has m=n=9 components.
//   In the Algo 566/paper, the problem has m=31.
//   The Scilab implementation is consistent with Algo 566/paper.
//
// Problem #25: Vardim
//   In Scilab and in Algo 566/paper, the problem has m=n+2 components.
//   In Algo 566/Fortran, the problem has m=n components.
//   The Scilab implementation is consistent with Algo 566/paper.
//

//
// Load this script into the editor
//
filename = "checkAlgo566_NLEQ.sce";
dname = get_absolute_file_path(filename);
editor ( dname + filename );

