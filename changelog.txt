changelog of the Unconstrained Optimization Problem Toolbox

uncprb (not released yet)
    * Increased portability of unit tests.
    * Vectorized bard, gauss, helix, jensam, meyer.

uncprb (0.2)
    * Increased portability of unit tests.
	* Added optimfun function to benchmark optim.
	* Removed auto-generated demos.
	* Added printsummary function to support benchmarks.
	* Added computestatus function to support benchmarks.
	* Updated toolbox to new assert module.
    * Added a benchmark for fminsearch
	* Added lsqrsolvefun and lsqrsolvejac to benchmark lsqrsolve.
    * Added leastsqfun and leastsqjac to benchmark leastsq.

uncprb (0.1)
    * Initial version
    * 0.1-2 : fixed portability bugs in the unit tests
    * rename to avoid name conflicts
    * create module architecture
    * format comments for auto-generation
    * create unit tests for uncprb_get* functions
    * fixed problem #27 : almost.sci is missing
    * fixed gradient of problem #31 : Broyden Banded
    * launch optim and fminsearch on these benchmarks
    * fixed ambiguity of problem 35:
	  -->uncprb_getname(35)  = rosex
	  but getproblems indicates chebyquad
    * suppressed the fvec = fvec'
    * suppressed the %sum
    * suppressed the zero = 0, one = 1, two = 2, etc...
    * checked conditions on n and m where appropriate
    * provided function value at optimum
    * provided optimum x when available
    * provided a classification
      * class 1 : Systems of Nonlinear Equations
      * class 2 : Non Linear Least Squares
      * class 3 : Unconstrained Minimization
    * create the overview in xml and generate the help
      * see .backup file and check against XXE
    * provided Hessian matrix when available (see algo 566)
    * checked exact Hessians with finite differences
    * run the fortran Algo 566 and compare the data
 -- Michael Baudin <michael.baudin@scilab.org>  May 2010

